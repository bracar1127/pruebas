﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using OrdenarNumeros;

namespace EjemploDll
{
    class Program
    {
        static void Main(string[] args)
        {
            OrdenadorNumerico objOrdenador = new OrdenadorNumerico();
            List<int> listaDesordenada = new List<int>();
            listaDesordenada.Add(2);
            listaDesordenada.Add(5);
            listaDesordenada.Add(1);
            listaDesordenada.Add(9);
            listaDesordenada.Add(4);
            listaDesordenada.Add(0);
            listaDesordenada.Add(3);
            listaDesordenada.Add(7);
            listaDesordenada.Add(6);
            listaDesordenada.Add(8);

            Console.WriteLine("Lista Desordenada:");
            Console.WriteLine();

            for (int i = 0; i <= listaDesordenada.Count - 1; i++)
            {
                Console.WriteLine(listaDesordenada[i].ToString());
            }

            Console.WriteLine();
            Console.WriteLine("Lista Ordenada descendente:");
            Console.WriteLine();

            listaDesordenada = objOrdenador.ordenarDescendente(listaDesordenada);

            for (int i = 0; i <= listaDesordenada.Count - 1; i++)
            {
                Console.WriteLine(listaDesordenada[i].ToString());
            }

            Console.WriteLine();
            Console.WriteLine("Lista Ordenada ascendente:");
            Console.WriteLine();

            listaDesordenada = objOrdenador.ordenarAscendente(listaDesordenada);

            for (int i = 0; i <= listaDesordenada.Count - 1; i++)
            {
                Console.WriteLine(listaDesordenada[i].ToString());
            }
            Console.ReadKey();
        }
    }
}
